# How to run the Foundation Services for local development

The code from our quick-start guides won't work if the Foundation Services aren't running. This SDK will instead return error messages every time you use one of its APIs.

There are a few ways you can start the Foundation Services:

## Running one Foundation Service locally

The simplest option is to pick just one Foundation Service and run it locally. This option is best if you're using the SDK to interact with just one service.

For example, if you want to spin up the [FDNS Object microservice](https://github.com/CDCGov/fdns-ms-object) locally, you can do that by issuing the following commands from a Bash terminal:

```bash
git clone https://github.com/CDCgov/fdns-ms-object.git
cd fdns-ms-object
make docker-build
make docker-start
```

> The `make docker-build` command will take a while to run because it requires pulling down hundreds of Java dependencies in order to compile the application.

It may take a few seconds, but you should now be able to visit `http://localhost:8083` in your browser to verify the service is running.

## Running all Foundation Services locally

One problem with the previous set of steps is that it only starts one microservice. What if you want to work with both the FDNS Object and FDNS Storage services? Or also the FDNS Indexing and FDNS Rules services? You can try starting all of these individually but you are very likely to encounter Docker conflicts and error messages.

Instead, you can clone the [FDNS Gateway microservice](https://github.com/CDCGov/fdns-ms-gateway) and start it locally. Doing so starts _all_ of the open source Foundation Services via a `docker-compose` script.

> You will need 16 GB or more of memory to reliably start all of the services.

The steps are quite similar to those shown above:

```bash
git clone https://github.com/CDCgov/fdns-ms-gateway.git
cd fdns-ms-gateway
make docker-build
make docker-start
```

## Running a subset of Foundation Services locally

An issue with using the [FDNS Gateway microservice](https://github.com/CDCGov/fdns-ms-gateway) is that to start all the Foundation Services will consume a lot of RAM. What you can do is open the `docker-compose.yml` file in the `fdns-ms-gateway` root repo (which you cloned from GitHub in the previous section) and commenting out any service calls you don't care about.

> Comments in `yml` files start with the `#` character.

Then, simply restart the `fdns-ms-gateway` container (you don't need to rebuild it):

```bash
make docker-restart
```


