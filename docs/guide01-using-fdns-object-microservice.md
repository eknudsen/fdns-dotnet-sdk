# How to use the SDK to interact with the FDNS Object microservice

The [FDNS Object microservice](https://github.com/CDCGov/fdns-ms-object) is an abstraction layer over a NoSQL database. It exposes several HTTP API endpoints that map to CRUD operations. This SDK uses generic programming so the FDNS Object microservice can work with any type of object.

You will need to have the following software installed to follow the instructions in this document:

- [Visual Studio Code](https://code.visualstudio.com/)
- [C# Extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
- [.NET Core SDK 2.1](https://www.microsoft.com/net/download)

> This guide assumes you're already running the FDNS Object microservice at `http://localhost:8083/api/1.0`. If not, you will receive errors when trying to use the code below.

In this example, we'll assume there is an object of type `Customer` that we wish to conduct CRUD operations on:

```cs
public sealed class Customer
{
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public string StreetAddress { get; set; }
    public DateTime DateOfBirth { get; set; }
}
```

Open `Startup.cs` in your ASP.NET Core 2 microservice and add the following lines to `ConfigureServices`:

```cs
services.AddHttpClient($"myAppName-{Common.OBJECT_SERVICE_NAME}", client =>
{
    client.BaseAddress = new Uri($"http://localhost:8083/api/1.0/bookstore/customer/");
    client.DefaultRequestHeaders.Add("X-Correlation-App", "myAppName");
})

services.AddSingleton<IObjectRepository<Customer>>(provider => new HttpObjectRepository<Customer>(
    provider.GetService<IHttpClientFactory>(),
    provider.GetService<ILogger<HttpObjectRepository<Customer>>>(),
    "myAppName"));
```

Also in `Startup.cs`, modify the `services.AddMvc()` method call to add the `TextPlainInputFormatter`. Doing so is required for implementing the SDK's Find API.

```cs
services.AddMvc(options =>
{
    options.InputFormatters.Add(new TextPlainInputFormatter());
})
.AddJsonOptions(options =>
{
    options.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
})
.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
```

Still in `Startup.cs`, add the following `using` directive:

```cs
using Foundation.Sdk.Data;
```

In your controller, add the SDK's `IObjectRepository` interface as a dependency that is injected via the constructor:

```cs
[Route("api/1.0")]
[ApiController]
public class CustomerController : ControllerBase
{
    private readonly IObjectRepository<Customer> _customerRepository;

    public CustomerController(IObjectRepository<Customer> repository)
    {
        _customerRepository = repository;
    }
    ...
}
```

Add the following helper method to the bottom of the controller:

```cs
private ActionResult<T> HandleObjectResult<T>(ServiceResult<T> result, string id = "")
{
    switch (result.Code)
    {
        case HttpStatusCode.OK:
            return Ok(result.Response);
        case HttpStatusCode.Created:
            return CreatedAtAction(nameof(Get), new { id = id }, result.Response);
        default:
            return StatusCode((int)result.Code, result.Message);
    }
}
```

Operations are fairly straightforward. To get an object by its id value:

```cs
[Produces("application/json")]
[HttpGet("{id}")]
public async Task<ActionResult<Customer>> Get([FromRoute] string id)
{
    ServiceResult<Customer> result = await _customerRepository.GetAsync(id);
    return HandleObjectResult<Customer>(result);
}
```

To insert an object with an explicit id:

```cs
[Produces("application/json")]
[HttpPost("{id}")]
public async Task<ActionResult<Customer>> Post([FromRoute] string id, [FromBody] Customer payload)
{
    if (!ModelState.IsValid)
    {
        return BadRequest(ModelState);
    }

    ServiceResult<Customer> result = await _customerRepository.InsertAsync(id, payload);
    return HandleObjectResult<Customer>(result, id);
}
```

To conduct a wholesale object replacement:

```cs
[Produces("application/json")]
[HttpPut("{id}")]
public async Task<ActionResult<Customer>> Put([FromRoute] string id, [FromBody] Customer payload)
{
    if (!ModelState.IsValid)
    {
        return BadRequest(ModelState);
    }

    ServiceResult<Customer> result = await _customerRepository.ReplaceAsync(id, payload);
    return HandleObjectResult<Customer>(result);
}
```

To delete an object:

```cs
[HttpDelete("{id}")]
public async Task<bool> Delete([FromRoute] string id)
{
    ServiceResult<bool> result = await _customerRepository.DeleteAsync(id);
    return HandleObjectResult<bool>(result);
}
```

Finding an object requires using the [MongoDB find syntax](https://docs.mongodb.com/manual/reference/method/db.collection.find/). An example of this syntax to find all customers with an age greater than 45: `{ age: { $lt: { 45 } }`. The `find` API also accepts from, size, sort, and sort direction criteria for easy pagination.

> ASP.NET Core requires that the find criteria be submitted as `text/plain`. You will need to implement a `TextPlainInputFormatter` to accept `text/plain`, otherwise ASP.NET Core will always view the payload as `null`. See the FDNS .NET Core Example microservice for how to implement `TextPlainInputFormatter`.

```cs
[Consumes("text/plain")]
[Produces("application/json")]
[HttpPost("find")]
public async Task<ActionResult<SearchResults<Customer>>> Find([FromBody] string findCriteria)
{
    ServiceResult<SearchResults<Customer>> result = await _customerRepository.FindAsync(0, 10, "name", findCriteria, false);
    return HandleObjectResult<SearchResults<Customer>>(result);
}
```

You can also get a Json array of distinct values for any given field in your collection. The aforementioned MongoDB find syntax is an optional parameter to this method if you wish to limit the scope of the operation. For example:

```cs
var distinctResult = await _customerRepository.GetDistinctAsync("id", findCriteria);
```