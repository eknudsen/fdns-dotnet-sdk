# FDNS .NET Core SDK Documentation

You will need to have the following software installed to use this SDK:

- [Visual Studio Code](https://code.visualstudio.com/)
- [C# Extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
- [.NET Core SDK 2.1](https://www.microsoft.com/net/download)

You can include the SDK by adding a package reference in your `csproj` file:

```xml
<PackageReference Include="Foundation.Sdk" Version="0.0.2" />
```

Visual Studio Code should prompt you to restore packages. If not, run `dotnet restore` from a command line.

## Quick-start guides:

* [How to run the Foundation Services for local development](guide00-starting-fdns-microservices.md)
* [Interacting with the FDNS Object microservice](guide01-using-fdns-object-microservice.md)
* [Interacting with the FDNS Storage microservice](guide02-using-fdns-storage-microservice.md)
* [Interacting with the FDNS Indexing microservice](guide03-using-fdns-indexing-microservice.md)